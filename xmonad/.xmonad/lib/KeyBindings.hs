module KeyBindings (
  KeyBindings.modify
  ) where

import XMonad
import XMonad.Actions.PhysicalScreens
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Layout.ToggleLayouts
import XMonad.Util.EZConfig
import qualified XMonad.StackSet as W

-- Non-XMonad imports
import qualified Data.Map as M
import System.Exit

-- Local imports
import MySettings

modify :: XConfig l -> XConfig l
modify conf = conf
  { modMask = myModMask --Win key
  }

  `additionalKeysP` myKeys

activeRecording = False
takeRecording :: Bool -> Bool
takeRecording x = if x 
                     then False
                     else True

-- Keybindings
myKeys =
  [
    ("M-<Return>", spawn myTerminal)
  , ("M-d", spawn myLauncher)
  , ("M-S-<Backspace>", kill)
  , ("M-<Space>", sendMessage NextLayout)
  , ("M-S-<Space>", sendMessage NextLayout)
  , ("M-n", refresh)
  , ("M-<Tab>", windows W.focusDown)
  , ("M-j", windows W.focusDown)
  , ("M-k", windows W.focusUp)
  , ("M-m", windows W.focusMaster)
  , ("M-S-<Return>", windows W.swapMaster)
  , ("M-S-j", windows W.swapDown)
  , ("M-S-k", windows W.swapUp)
  , ("M-h", sendMessage Shrink)
  , ("M-l", sendMessage Expand)
  , ("M-t", withFocused $ windows . W.sink)
  , ("M-,", sendMessage (IncMasterN 1))
  , ("M-.", sendMessage (IncMasterN (-1)))
  , ("M-b", sendMessage ToggleStruts)
  , ("M-S-q", io (exitWith ExitSuccess))
  , ("M1-S-r", spawn "xmonad --recompile; xmonad --restart")
  , ("M-f", sendMessage (Toggle "Full"))
  , ("M-p", spawn "rofi -modi 'clipboard:greenclip print' -show clipboard -run-command '{cmd}'")
  , ("M-o", spawn "rofimoji")
  , ("M-u", spawn "rofi-screenshot")
  , ("M-S-u", spawn "rofi-screenshot -s")

  ]
  ++
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
  [ (mask ++ "M-" ++ [key], screenWorkspace scr >>= flip whenJust (windows . action))
    | (key, scr)  <- zip "wer" [2,0,1] -- was [0..] *** change to match your screen order ***
    , (action, mask) <- [ (W.view, "") , (W.shift, "S-")]
  ]
