module MyLayouts (myLayouts) where

import XMonad
import XMonad.Hooks.ManageDocks

import XMonad.Layout.Accordion
import XMonad.Layout.NoBorders (noBorders, smartBorders)
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.ThreeColumns (ThreeCol(..))
import XMonad.Layout.ToggleLayouts (toggleLayouts)
import XMonad.Layout.WindowNavigation

myLayouts = avoidStruts (tall ||| Mirror tall ||| threeCol ||| Full )
  where
    tall = Tall 1 (3/100) (1/2)
    threeCol = ThreeColMid 1 (1/10) (1/2)

