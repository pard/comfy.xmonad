module MySettings where

import XMonad

myTerminal    = "kitty"
myLauncher    = "rofi -combi-modi window,drun -show combi -modi combi -show-icons"
myModMask     = mod4Mask
myBorderWidth = 3
