{-# LANGUAGE FlexibleContexts #-}

module StatusBar (
  StatusBar.modify
) where

import Data.List
import Data.Monoid

import XMonad.Core
import XMonad.Hooks.DynamicBars (dynStatusBarStartup,dynStatusBarEventHook,multiPP)
import XMonad.Hooks.DynamicLog
import XMonad.ManageHook
import XMonad.Util.Run

import ColorScheme as C

-- Type constructors
import GHC.IO.Handle (Handle)

modify :: XConfig l -> XConfig l
modify conf = conf
  { startupHook = do
      startupHook conf
      dynStatusBarStartup runXmobar killXmobar
  , handleEventHook = handleEventHook conf <+> dynStatusBarEventHook runXmobar killXmobar
  , logHook = do
    logHook conf
    multiPP currentPP otherPP
  }

runXmobar :: ScreenId -> IO Handle
runXmobar (S id) = spawnPipe
  $  "xmobar -x"
  <> show id
  <> " $HOME/.xmonad/xmobarrc"

killXmobar :: IO ()
killXmobar = return ()

otherPP = currentPP
  { ppCurrent         = xmobarColor C.foreground   C.background
  , ppVisible         = xmobarColor C.foreground   C.background
  , ppHidden          = xmobarColor C.foreground   C.background
  , ppHiddenNoWindows = xmobarColor C.backgroundhl C.background
  }

currentPP = def
  { ppCurrent         = xmobarColor' C.orange
  , ppVisible         = xmobarColor' C.yellow       -- other screen
  , ppHidden          = xmobarColor' C.foreground   -- other workspaces with windows
  , ppHiddenNoWindows = xmobarColor' C.foregroundll -- other workspaces
  , ppSep             = "  "
  , ppWsSep           = " "
  , ppLayout          = printLayout
  , ppTitle           = printTitle
  }
  where xmobarColor' fg = xmobarColor fg C.background

        printLayout s | "Mirror Tall" `isSuffixOf` s = "[ — ]"
        printLayout s | "Tall" `isSuffixOf` s = "[ | ]"
        printLayout "Full" = "[ F ]"
        printLayout l = "[" ++ l ++ "]"

        printTitle t = let t' = shorten 120 t
                       in xmobarColor' C.foregroundll "« "
                       ++ xmobarColor' C.foregroundhl t'
                       ++ xmobarColor' C.foregroundll " »"


