module Main (main) where

import XMonad

-- XMonad Hook imports
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers

import XMonad.Config.Desktop

-- XMonad Layout imports

-- XMonad Util imports
import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Util.SpawnOnce

import qualified XMonad.StackSet as W

-- Local imports
import MySettings
import KeyBindings as Keys (modify)
import MyLayouts (myLayouts)
import StatusBar as Bar (modify)

-- Non-XMonad imports
import qualified Data.Map as M
import System.Exit

-- Local imports
import MySettings

-- Startup Hooks
myStartupHook = do
  spawnOnce "$HOME/.xmonad/initialize"

-- Manage Hooks
myManageHook = composeOne
  [ className =? "Microsoft Teams Notification" -?> doRectFloat (W.RationalRect 0.35 0.35 0.35 0.35)
  , className =? "Pavucontrol" -?> doRectFloat (W.RationalRect 0.35 0.35 0.35 0.35)
  , isDialog -?> doCenterFloat
  , transience
  ]

withConfig =
  Keys.modify
  $ Bar.modify
  $ desktopConfig
  {
    borderWidth     = 1
  , layoutHook      = desktopLayoutModifiers myLayouts
  , manageHook      = myManageHook <+> manageHook desktopConfig
  , modMask         = myModMask
  , handleEventHook = handleEventHook desktopConfig
  , startupHook     = myStartupHook <+> startupHook desktopConfig
  , terminal        = myTerminal
  }

main = do
  xmonad withConfig
